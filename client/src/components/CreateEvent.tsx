import 'react-app-polyfill/ie11';
import * as React from 'react';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import { EventCreate } from '../interfaces/EventData';

import Button from '@mui/material/Button';
import TextField from 'material-ui/TextField';

const NewEventSchema = Yup.object().shape({
  title: Yup.string()
    .max(100, 'Too long!')
    .required('Required'),
  from_datetime: Yup.date()
    .required('Required'),
  to_datetime: Yup.date()
    .required('Required'),
  location: Yup.string()
    .max(1000, 'Too long!'),
  details: Yup.string()
    .max(1000, 'Too long!'),
});

const validateDate = async (values: EventCreate) => {
  const errors: any = {};
  if (values.from_datetime > values.to_datetime){
    errors.to_datetime = 'End datetime should not be earlier than start datetime.';
  }
  return errors;
}

export const CreateEvent = (props: any) => {
  return (
    <div>
      <h1>Create new event!</h1>
      <Formik
        initialValues={{
            title: '',
            from_datetime: '',
            to_datetime: '',
            location: '',
            details: '',
        }}
        validationSchema={NewEventSchema}
        validate={validateDate}
        onSubmit={props.onSubmit}
      >
        {({ errors, touched }) => (
          <Form>
            <label htmlFor="title">Title </label>
            <Field id="title" name="title" placeholder="Example event" fullWidth/>
            {/* If this field has been touched, and it contains an error, display it
            */}
           {touched.title && errors.title && <div style={{ color: 'red' }}>{errors.title}</div>}
            <br/>

            <label htmlFor="fromDatetime">Start date/time </label>
            <Field id="from_datetime" name="from_datetime" placeholder="" type="datetime-local" />
            {/* If this field has been touched, and it contains an error, display it
            */}
           {touched.from_datetime && errors.from_datetime && <div style={{ color: 'red' }}>{errors.from_datetime}</div>}
            <br/>

            <label htmlFor="toDatetime">End date/time </label>
            <Field id="to_datetime" name="to_datetime" placeholder="" type="datetime-local" />
            {/* If this field has been touched, and it contains an error, display it
            */}
           {touched.to_datetime && errors.to_datetime && <div style={{ color: 'red' }}>{errors.to_datetime}</div>}
            <br/>

            <label htmlFor="location">Location </label>
            <Field id="location" name="location" placeholder="Example address" />
            {/* If this field has been touched, and it contains an error, display it
            */}
           {touched.location && errors.location && <div style={{ color: 'red' }}>{errors.location}</div>}
            <br/>

            <label htmlFor="details">Details </label>
            <Field id="details" name="details" placeholder="Example details for event" />
            {/* If this field has been touched, and it contains an error, display it
            */}
           {touched.details && errors.details && <div style={{ color: 'red' }}>{errors.details}</div>}
            
            <br/>
            <Button color="primary" variant="contained" type="submit">Submit</Button>
          </Form>
        )}
      </Formik>
    </div>
  );
};