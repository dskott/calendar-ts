import { EventCreate, EventFullDetails, EventWithId, EventWithIdDatabase } from "../interfaces/EventData";

export class CalendarService {
    public async getEvents(): Promise<EventWithId[]> {
        const response = await fetch('event/all');
        const apiJsonResponse = await response.json();

        const convertFromDbType = (apiJsonResponse: any) => {
            const events = apiJsonResponse.data;
            let output: EventWithId[] = [];
            events.forEach((event: EventWithIdDatabase) => {
                output.push({
                    eventId: event.eventId,
                    title: event.title,
                    start: new Date(event.from_datetime),
                    end: new Date(event.to_datetime),
                })
            })
            return output;
        };
        
        return convertFromDbType(apiJsonResponse);
    }

    public async getEventDetails(eventId: number): Promise<EventFullDetails> {
        const response = await fetch(`event/${eventId}`);
        const apiJsonResponse = await response.json();

        const output: EventFullDetails = {
            title: apiJsonResponse.data.title,
            start: new Date(apiJsonResponse.data.from_datetime),
            end: new Date(apiJsonResponse.data.to_datetime),
            details: apiJsonResponse.data.details,
            location: apiJsonResponse.data.location
        };

        return output;
    }

    public async postEvent(event: EventCreate): Promise<any> {
        const response = await fetch(`/event/create`, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(event)
          })
        return await response.json();
    }
}