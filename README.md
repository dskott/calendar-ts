## Setup
### Download repo, deps and build
- Clone the repo locally
- `$ cd calendar-ts/`
- `$ cd server/ && npm install && npm run build && cd ..`
- `$ cd client/ && npm install && cd ..`

### Set up mysql database
- `$ cd calendar-ts/`
- Sign in to local mysql db as root `$ sudo mysql`
- Run the SQL script `$ source ./sql-create-datastore-commands.sql;`

### Start servers
- Run backend server `$ cd calendar-ts/server/ && node lib/index.js`
- Open another terminal and run frontend developer server `$ cd calendar-ts/client/ && npm start`
- Open your browser and navigate to `http://localhost:3000/` (it may take a while to load until the development server starts)

### Usage instructions
- View all events: Use the buttons at the top "back", "next" to navigate to different days/weeks/months to view the scheduled events
- Create new event: fill the form fields at the bottom of the page (e.g. title, start date/time etc) and click submit.
- View event details: click on an event in the calendar

## Project requirements
- Single page app
- Create new event
- View list of past and future events
- Click on event to view details
- Create RESTful API server to get the data into the app
- Optional:
    - Dislplay events in calendar form
    - Delete event
    - Update event
    - Sanitize input
    - Recurring events
    - Reminders
    - Drag and drop events on calendar to change their start date

## Design decisions - Choices of technologies
### Technologies
- Data storage: MySQL DB -> using a SQL DB because data has consistent schema and MySQL is freely available
- Back-end: Typescript with NodeJs -> Very scalable backend for I/O intensive applications like this one (mostly involves a server interacting with the DB). Also very rich ecosystem of libraries with npm
- Front-end: Typescript with React. It is a stable library, very popular with a huge community, easier to learn than other tools (e.g. angular)
- Libraries: 
    - Server: `express` -> very easy to create routes, used it before
    - Interact with mysql: `mysql2` -> very easy to use, used it before
    - Display events in calendar form: `react-big-calendar` -> has the ability to display events on top of a weekly calendar view, and has good documentation
    - Form submission in react: `formik` -> takes care of creating and submitting a form in react, as well as input validation

### Trade-offs, design decisions
- Database design: is it better to keep all the data associated with an event in a single table, or should it be split to 2 tables; the required event fields (i.e. title, start date, end date) and the optional fields (description, location), which can be NULL a lot of the time?
    - Use the first option, since it is a less complex design, and it will never require joins. If in the future, we need to optimize the database size, we can split the data into 2 tables: the required and the optional event fields, so that we don't waste space for NULL values
- Using timestamp columns in mysql instead of datetime columns, to preserve timezone information and set database to using UTC, see https://medium.com/@kenny_7143/time-zone-in-mysql-e7b73c70fd4e and https://stackoverflow.com/questions/930900/how-do-i-set-the-time-zone-of-mysql/19069310 
- Shortcut: Display details of an event in the front-end using a browser alert, to save on development time. A browswer alert is not a very user-friendly way of displaying this information. Normally, I would create a new react component to display the details
- Shortcut: The "create new event" form fields are always present at the bottom of the page. This is because it was the simplest way I could think of with my current knowledge of React. If I had time, I would have added a button for creating new events, that when clicked would re-paint the DOM with the "create new event" form fields
- I have noted certain optional improvements to this basic calendar application, under "Project requirements" above. If I had more time I would have implemented API endpoints for updating and deleting events, and I would have used those in the front-end
