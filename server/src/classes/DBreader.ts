import mysql from "mysql2";
import { OkPacket, RowDataPacket } from "mysql2";
import * as dotenv from "dotenv";
import { eventBasic, eventFullInfo } from "../interfaces/apiTypes";

dotenv.config();

export class DBreader {
    private db: mysql.Connection;

    constructor(){ 
        this.db = mysql.createConnection({
            host: process.env.DB_HOST,
            user: process.env.DB_USER,
            password: process.env.DB_PWD,
            database: process.env.DB_NAME,
            timezone: process.env.DB_TIMEZONE
        });
    }

    getAllEvents(callback: Function, from?: Date, to?: Date){
        // if any of the given dates from, to is not a valid date, 
        // it will not be considered in the query

        // add filter to query based on from, to
        let filterString = "";
        let filterParams = [];

        if ((from) || (to)){
            filterString = " WHERE 1=1";
            if ((from) && this.isValidDate(from)){
                filterString += `
                AND end_ts > ?`
                filterParams.push(from.toISOString()); // convert to UTC
            }
            if ((to) && this.isValidDate(to)) {
                filterString += `
                AND start_ts <= ?`
                filterParams.push(to.toISOString());
            }
        }

        const queryString = `SELECT
        eventId, title, start_ts, end_ts 
        FROM events`
        + filterString;

        this.db.query(
            queryString,
            filterParams,
            (err, result) => {
                if (err) {
                    callback(err, null)
                    console.log("Error reading from db");
                    return;
                };
                
                const rows = <RowDataPacket[]> result;
                const events: eventBasic[] = [];

                rows.forEach(row => {
                    const event: eventBasic = {
                        eventId: row.eventId,
                        title: row.title,
                        from_datetime: new Date(row.start_ts),
                        to_datetime: new Date(row.end_ts)
                    }
                    events.push(event);
                });
                callback(null, events);  
            });
    }

    getDetailedEvent(id: number, callback: Function){
        const queryString = `
        SELECT
        eventId, title, start_ts, end_ts, details, location  
        FROM events
        WHERE eventId=?`;
        const filterParams = [id];

        this.db.query(
            queryString,
            filterParams,
            (err, result) => {
                if (err) {
                    callback(err, null)
                    console.log("Error reading from db");
                    console.log(err);
                    return;
                };
                
                const rows = <RowDataPacket[]> result;
                if (rows.length === 0) {
                    callback(new Error("No event with the given id exists. "), null);
                    return;
                }

                const row = rows[0];
                const event: eventFullInfo = {
                    eventId: row.eventId,
                    title: row.title,
                    from_datetime: new Date(row.start_ts),
                    to_datetime: new Date(row.end_ts),
                    details: row.details,
                    location: row.location
                };
                
                callback(null, event);  
            });
    }

    persistEvent(event: eventFullInfo, callback: Function){
        const queryString = `
        INSERT INTO events (
            title,
            start_ts,
            end_ts,
            details, 
            location
        )
        VALUES 
            (?, STR_TO_DATE(?, '%Y-%m-%dT%H:%i:%s.%fZ'), STR_TO_DATE(?, '%Y-%m-%dT%H:%i:%s.%fZ'), ?, ?)`;
        const filterParams = [event.title, event.from_datetime, 
            event.to_datetime, event.details, event.location];

        this.db.query(
            queryString,
            filterParams,
            (err, result) => {
                if (err) {
                    callback(err, null);
                    console.log("Error writing to db");
                    console.log(err);
                    return;
                } else {
                    const insertId = (<OkPacket> result).insertId;
                    callback(null, insertId);
                };
            });
    }

    isValidDate(d: Date){
        return (Object.prototype.toString.call(d) === '[object Date]') && (!isNaN(d.valueOf()));
    }
}