import { Event } from 'react-big-calendar'

export interface EventWithId extends Event {
    eventId: number
}

export interface EventWithIdDatabase {
    eventId: number
    title: string
    from_datetime: Date
    to_datetime: Date
}

export interface EventFullDetails extends Event {
    details: string
    location: string
}

export interface EventCreate {
    title: string,
    from_datetime: string|Date,
    to_datetime: string|Date,
    details: string, 
    location: string
}