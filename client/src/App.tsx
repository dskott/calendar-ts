import { FC, useEffect, useState } from 'react'
import { Calendar, dateFnsLocalizer } from 'react-big-calendar'
import format from 'date-fns/format'
import parse from 'date-fns/parse'
import startOfWeek from 'date-fns/startOfWeek'
import getDay from 'date-fns/getDay'
import enGB from 'date-fns/locale/en-GB'

// calendar styling
import 'react-big-calendar/lib/addons/dragAndDrop/styles.css'
import 'react-big-calendar/lib/css/react-big-calendar.css'

// integrating with API
import { CalendarService } from './services/CalendarService'
import {EventFullDetails, EventCreate, EventWithId} from './interfaces/EventData'
import { CreateEvent } from './components/CreateEvent'

const App: FC = () => {
  // define React state variables for the app
  const [events, setEvents] = useState<EventWithId[]>([])
  const [updateEvents, setUpdateEvents] = useState<boolean>(true);

  const eventService = new CalendarService();

  useEffect(() => {
    const getAllEvents = async () => {
      const events = await eventService.getEvents();
      setEvents(() => {
        return events;
      });
    }
    if (updateEvents) {
      getAllEvents();
      setUpdateEvents(() => false);
    }
  }, [updateEvents]);

  const onSelectEvent = (event: EventWithId) => {
    const processEvents = async (eventId: number) => {
      const eventWithDetails: EventFullDetails = await eventService.getEventDetails(event.eventId);
      
      let outputString = "";
      for (let [key, value] of Object.entries(eventWithDetails)) {
        outputString += `${key}: ${value} \n\n`;
      }
      
      alert(outputString);
    }
    processEvents(event.eventId)
  }

  const onCreateNewEvent = (event: EventCreate) => {
    const persistEvent = async (event: EventCreate) => {
      // convert dates to UTC since the backend expects UTC dates
      event.from_datetime = new Date(event.from_datetime).toISOString();
      event.to_datetime = new Date(event.to_datetime).toISOString();

      const response = await eventService.postEvent(event);
      setUpdateEvents(() => true); // ensure calendar gets updated

      // check if backend returns an error
      if (response.message){
        alert(response.message);
      }
    }

    persistEvent(event);
  }

  return (
    <div>
      <Calendar
        selectable
        defaultView='week'
        events={events}
        localizer={localizer}
        onSelectEvent={onSelectEvent}
        style={{ height: '75vh' }}
      />

      <CreateEvent 
        onSubmit={onCreateNewEvent}
      />
    </div>
  ) 
}



const locales = {
  'en-GB': enGB,
}

// The types here are `object`. Strongly consider making them better as removing `locales` caused a fatal error
const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek,
  getDay,
  locales,
})

export default App;
