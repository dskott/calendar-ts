import express, {Request, Response} from "express";
import { DBreader } from "../classes/DBreader";
import { eventBasic, eventFullInfo } from "../interfaces/apiTypes";

const eventsRouter = express.Router();
const dbReaderLocal = new DBreader();

eventsRouter.get("/all", async (req: Request, res: Response) => {
    // in the http request, the 2 query parameters should be separated with '\&'
    const from = new Date(String(req.query.startDate));
    const to = new Date(String(req.query.endDate));
    console.log("GET event/all?" + "startDate=" + from + "&endDate=" + to);

    if (from > to){
        return res.status(500).json({"message": "startDate should not be larger than endDate"});
    }

    dbReaderLocal.getAllEvents((err: Error, events: eventBasic[]) => {
        if (err) {
            return res.status(500).json({"message": err.message});
        }
        res.status(200).json({"data": events});
    }, from, to);
})

eventsRouter.get("/:id", async (req: Request, res: Response) => {
    const id = Number(req.params.id);
    console.log("GET event/" + id);

    dbReaderLocal.getDetailedEvent(id, (err: Error, event: eventFullInfo) => {
        if (err) {
            return res.status(500).json({"message": err.message});
        }
        res.status(200).json({"data": event});
    });
})

eventsRouter.post("/create", async (req: Request, res: Response) => {
    console.log("POST event/create");
    const event: eventFullInfo = req.body;

    if ((event.from_datetime) && (event.to_datetime) && (event.from_datetime > event.to_datetime)){
        return res.status(500).json({"message": "from_datetime cannot be larger than to_datetime"});
    }

    dbReaderLocal.persistEvent(event, (err: Error, eventId: number) => {
        if (err) {
            return res.status(500).json({"message": err.message});
        }
        res.status(200).json({"eventId": eventId});
    });
})

export {eventsRouter};