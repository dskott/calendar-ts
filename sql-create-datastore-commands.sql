/* Sign in with database admin user */

create database calendar;
SET @@global.time_zone = '+00:00';
CREATE USER 'calendarUser'@'localhost' IDENTIFIED BY '';
GRANT SELECT,UPDATE,INSERT,DELETE ON calendar.* TO 'calendarUser'@'localhost';

USE calendar;
/* Create events table */
CREATE TABLE events (
    eventId BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(100) NOT NULL,
    start_ts TIMESTAMP NOT NULL,
    end_ts TIMESTAMP NOT NULL,
    details VARCHAR(1000),
    location VARCHAR(1000)
);

/* Add example data */
INSERT INTO events (
    title,
    start_ts,
    end_ts,
    details
)
VALUES 
    ("My very first event!", "2021-02-19 10:00:00", "2021-02-19 11:00:00", NULL),
    ("My second event!", "2021-02-21 20:00:00", "2021-02-21 22:00:00", "This aim of this event is..."),
    ("Doctor", "2021-11-22 10:30:00", "2021-11-22 11:00:00", "Check my cold symptoms"),
    ("Workout", "2021-11-24 16:00:00", "2021-11-24 17:00:00", "Front lever, Thoracic bridge, Mobility"),
    ("Workout", "2021-11-25 16:00:00", "2021-11-25 17:00:00", "Side lever, Obliques mobility"),
    ("Meet John for coffee", "2021-11-26 15:00:00", "2021-11-26 18:00:00", "")
;