import express from "express";
import * as bodyParser from "body-parser";
const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');
import {eventsRouter} from "./routes/eventsRouter";
import { DBreader } from "./classes/DBreader";
import { eventBasic } from "./interfaces/apiTypes";

// Show starting screen
clear();
console.log(
  chalk.red(
    figlet.textSync('calendar-server', { horizontalLayout: 'full' })
  )
);


// start server
const app = express();
app.use(bodyParser.json());
app.use("/event", eventsRouter);

const serverPort: number = 8000;
app.listen(serverPort, () => {
  console.log("Node server started running");
});