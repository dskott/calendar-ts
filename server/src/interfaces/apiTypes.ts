export interface eventBasic {
    eventId: number,
    title: string, 
    from_datetime: Date,
    to_datetime: Date
}

export interface eventFullInfo extends eventBasic {
    details: string
    location: string
}